import React from "react";
import {StatusBar} from "react-native";
import Color from "react-native-material-color";
import moment from "moment";
export default class Utils {
    static uuid = () => {
        var d = new Date().getTime();
        if (typeof performance !== 'undefined' && typeof performance.now === 'function'){
            d += performance.now(); //use high-precision timer if available
        }
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = (d + Math.random() * 16) % 16 | 0;
            d = Math.floor(d / 16);
            return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
        });
    }

    static currentDate =   () => {
        const date = new Date();
        return moment(date).startOf('day').toISOString();
    }
    static renderStatusBar = () => {
        return <StatusBar backgroundColor = {Color.GREEN[800]} />
    }
    static getMonthBound = (date) =>  {
            const startDate = moment(date).startOf('month').startOf('day').toISOString();
            const endDate = moment(date).endOf('month').endOf('day').toISOString();
            return {startDate, endDate};
    }
    static getTitle = () => {
            titles = [
                'Ranjeet fee',
                'Pihu dress',
                'Music player',
                'Tomato',
                'Potato',
                'Big bazar',
                'Bannan',
                'Ring of king',
                'Sugar',
                'Medican pihu',
                'Medican home',
                'Gas',
                'Bicycle',
                'Bike',
                'Petrol',
                'Petrol',
                'Petrol bike',
                'petrol car',
                'petrol papa',
                'Ino b',
                'Ino home',
                'Ice cream',
                'Juice',
                'Slice',
                'Mango',
                'Rasana',
                'Pink tub',
                'Juicer',
                'Ranjeet',
                'Rekha',
                'Chai',
                'Goddday',
                'go daday',
                'Cloths',
                'Machine',
                'Ram singh',
                'Churan',
                'Pudina',
                'Machana',
                'Sanjay',
                'Mithoo',
            ];
        index = Math.floor(Math.random() * titles.length);
        return titles[index];
    }

    static random = (min, max) => {
        return Math.floor(Math.random() * (max - min + 1) + min);
    }
    static toAmt = (amt) => {
        return parseFloat(parseFloat(amt).toFixed(2));
    }
}
