import React, {Component} from "react";
import {Text, View, StyleSheet, Image, 
    StatusBar, SectionList,
    TouchableOpacity,
    ActivityIndicator,
} from "react-native";
import moment from "moment";
import Color from "react-native-material-color";
import TCard from "../components/TransactionCard";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import Utils from "../Utils";
import sectionListGetItemLayout from "react-native-section-list-get-item-layout";
import {Transaction} from "../models";
import SectionListHeader from "../components/SectionListHeader";


export default class AllTransactionsScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            test: '',
            sectionIndex: 0,
            itemIndex: 0,
            duration: (new Date()).toISOString(),
            trans: [
            ],
            loading: false,
        }
        this.getItemLayout = sectionListGetItemLayout({
            getItemHeight : () => 65,
            getSectionHeaderHeight : () => 30,
        });
    }
    static navigationOptions = {
        headerStyle: {
            // elevation: 0,
            backgroundColor: Color.GREEN[700], 
        }
    }
    componentWillMount() {
        console.log('setup date - ' + this.state.duration);
        this.focusSubscription = this.props.navigation.addListener('willFocus', this._onFocus);
    }
    componentWillUnmount () {
        if (this.focusSubscription) {
            this.focusSubscription.remove();
        }
    }
    _onFocus = () => {
        this._updatePageData(this.state.duration);        
    }
    _updatePageData = (date) => {
        Transaction.getAllTransactions(date, undefined, data => {
            this.setState({ trans: data});
        });
    }
    _monthDown =  () => {
        this.setState({duration: moment(this.state.duration).subtract(1, 'months').toISOString()}, () => {
            this._updatePageData(this.state.duration);
            if (this.state.trans.length > 0) {
                this.refs.sectionlist.scrollToLocation({sectionIndex: 0, itemIndex: 0, viewOffset: 40});
            }
        });
    }

    _monthUp =  () => {
        // alert('down');
        // this.setState({loading: true});
        this.setState({duration: moment(this.state.duration).add(1, 'months').toISOString()}, () => {
            this._updatePageData(this.state.duration);
            if (this.state.trans.length > 0) {
                this.refs.sectionlist.scrollToLocation({sectionIndex: 0, itemIndex: 0, viewOffset: 40});
            }
        });
    }

    _onTransactionClick = (trans) => {
        // alert(JSON.stringify(trans));
        // this.props.navigation.navigate({key: 'editTransaction', routeName: 'EditTransaction', params: {id: trans.id, updateList: this._updatePageList, pageData: {}}});
        this.props.navigation.navigate({
                key: 'editTransaction', 
                routeName: 'EditTransaction', 
                params: {
                        transaction: trans, 
                        updateList: this._updatePageList, 
                        pageData: {date: this.state.duration}}});

    }

    _updatePageList = (pageData) => {
        // alert(JSON.stringify(pageData));
        this._updatePageData(pageData.date);
    }

     _renderItem = ({item}) => {
        return (
            <TCard
                data = {item}
                onPress = {() => this._onTransactionClick(item)}
            ></TCard>
        )
    }
    _renderMonthSelector = (loading) => {
        if (loading) {
            return (
                <View style ={styles.spinner}>
                    <ActivityIndicator color = {Color.GREEN[700]} size={30}/>
                </View>
            )
        } 
        return (
            <View style={styles.monthSelector}>
                <TouchableOpacity  style={styles.monthSelectorButton} onPress={this._monthDown}>
                    <Icon name="chevron-left" size={24} style={{}}></Icon>
                </TouchableOpacity>
                <Text style={{fontWeight: 'bold', fontSize: 20}}>{moment(this.state.duration).format('MMMM YYYY')}</Text>
                <TouchableOpacity  style={styles.monthSelectorButton} onPress={this._monthUp}>
                    <Icon name="chevron-right" size={24} style={{}}></Icon>
                </TouchableOpacity>
            </View>
        )
    }

   render() {
        return (
            <View style={styles.container} >
                {/* <Text>{this.state.loading ? 'yes' : 'no'}}</Text> */}
                { Utils.renderStatusBar()}
                { this._renderMonthSelector(this.state.loading)}
                    <SectionList
                        ref = "sectionlist"
                        keyExtractor = {(item, index) => 'key-' + index}
                        style={styles.sectionList}
                        sections = {this.state.trans}
                        renderItem = {this._renderItem}
                        renderSectionHeader= {({section}) => <SectionListHeader section={section} />}
                        initialNumToRender={20}
                        stickySectionHeadersEnabled={true}
                        getItemLayout = {this.getItemLayout}
                    ></SectionList>
            </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    monthSelector: {
        backgroundColor: Color.GREY[400],
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingVertical: 12,
        paddingHorizontal: 18,
    },
    spinner: {
        backgroundColor: Color.GREY[400],
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 12,
    },

    sectionList: {
        // flex: 1,
    },

});