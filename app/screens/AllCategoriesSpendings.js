import React, {Component} from "react";
import {Text, View, StyleSheet, FlatList} from "react-native";
import TopSpendingCard from "../components/TopSpendingCard";

import {Transaction} from "../models";


export default class AllCategoriesSpendings extends Component {
    constructor(props) {
        super(props);
        this.state = {
            date: new Date().toISOString(),
            cats: {},
        }
    }

    componentWillMount() {
        Transaction.getCategoryWiseSpendings(this.state.date, data => {
            this.setState({cats: data});
            // console.log(data);
        });
    }
    _renderItem = (item, gtotal) => {
        return <TopSpendingCard
                    item = {item}
                    gtotal ={gtotal}
                    onPress = {() => this._onCategoryClick(item)}
                ></TopSpendingCard>
    }

    _onCategoryClick = (item) => {
        this.props.navigation.navigate({key: 'categorySpendings', routeName: 'CategorySpendings', params: {category: item}});
    }
    render() {
        const gtotal = Object.values(this.state.cats).reduce((r, i) => r = r + i.expense, 0);
        return (
            <View style={styles.container}>
                <FlatList
                    keyExtractor = {(item) => "key-" + item.id}
                    data = {this.state.cats}
                    renderItem = {({item}) => this._renderItem(item, gtotal)}
                    initialNumToRender={10}
                ></FlatList>
            </View>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});