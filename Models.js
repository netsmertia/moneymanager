
const Wallet = {
    name: 'Wallet',
    primaryKey: 'id',
    properties: {
        id: 'string',
        walletName: 'string',
        walletType: 'string',
        walletBalance: {type: 'int', default: 0},
        walletIcon: 'string',
    }
}

const Category = {
    name: 'Category',
    primaryKey: 'id',
    properties: {
        id: 'int',
        categoryName: 'string',
        iconFile: 'string',
        parentId: {type: 'int', default: 0},
        deleted: {type: 'bool', default: false},
        type: {type: 'string', default: 'expense'}
    },
};

const Transaction = {
    name: 'Transaction',
    primaryKey: 'id',
    properties: {
        id: 'string',
        title: 'string?',
        category: 'Category',
        amt: 'double',
        date: 'date',
        createdAt: 'date',
        deleted: {type: 'bool', default: false},
        wallet: {type: 'Wallet'},
    }
}

export {Category, Transaction, Wallet};