import DBHelper from "./DBHelper";
import Config from "../Config";
import {WalletSchema} from "./Schema"; 
import Utils from "../Utils";
import Transaction from "./Transaction";

export default class Wallet {


    static getAllWallets = (callback) => {
        DBHelper.open(realm => {
            const data = realm.objects(WalletSchema.name);
            callback(data);
        });
    }

    static getWalletWiseTotal = (date,callback) => {
        DBHelper.open(realm => {
            const startTime = +new Date();
            const {startDate, endDate} = Utils.getMonthBound(date);
            rows = realm.objects('Transaction').filtered('deleted == false and date >= $0 and date <= $1', startDate, endDate);
            data = rows.reduce((r, item) =>{
                var wallet = item.wallet;
                r[wallet.name] = r[wallet.name] || {
                    id: wallet.id,
                    walletName: wallet.name,
                    walletIcon: wallet.icon,
                    walletType: wallet.type,
                    walletTotal: 0,
                    inflow: 0,
                    outflow: 0,
                };
                r[wallet.name].walletTotal += item.amt;
                switch(item.type) {
                    case Config.transactionType.expense:
                        r[wallet.name].outflow += item.amt;
                        break;
                    case Config.transactionType.income:
                        r[wallet.name].inflow += item.amt;
                        break;
                    case Config.transactionType.transfer:
                        r[wallet.name].outflow += item.amt;
                        const toWallet = item.toWallet;
                        r[toWallet.name] = r[toWallet.name] || {
                                    id: wallet.id,
                                    walletName: wallet.name,
                                    walletIcon: wallet.icon,
                                    walletType: wallet.type,
                                    walletTotal: 0,
                                    inflow: 0,
                                    outflow: 0,
                        }
                        r[toWallet.name].inflow += item.amt;
                }
                return r;
            }, {});
            const endTime = +new Date();
            callback(data);
        });
    }

    static setupInitialWallet = (callback) => {
        DBHelper.open(realm => {
            realm.write(() => {
                const wallet = realm.objects('Wallet');
                if ( wallet.length > 0) {
                    return 0;
                } else {
                    const data = Config.defaultWallets;

                    data.map((value, index) => {
                        realm.create('Wallet', {
                            id: Utils.uuid(),
                            ...value
                        });
                    });
                    callback();
                }
            })
        }) 
    }
}