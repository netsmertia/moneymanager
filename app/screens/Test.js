import React, {Component} from "react";
import {View, Text, StyleSheet, TextInput, Button, ScrollView} from "react-native";
import firebase from "react-native-firebase";
export default class TestScreen extends Component {
    constructor(props) {
        super(props);
        this.unsubscriber = null;
        this.state = {
            email: '',
            password: '',
            isRegistered: false,
            user: {},
            additionalInfo: {},
        }
    }

    componentWillMount() {
        this.setState({user: firebase.auth().currentUser});
        this.unsubscriber = firebase.auth().onAuthStateChanged(user => {
            this.setState({user: user});
        });

    }
    componentWillUnmount() {
        if (this.unsubscriber) {
            this.unsubscriber();
        }
    }
    _singInOrLoginIn = () => {
        if (this.state.isRegistered) {
            //sign in
            // firebase.auth().
            firebase.auth().signInAndRetrieveDataWithEmailAndPassword(
                this.state.email,
                this.state.password
            ).then(data =>{
                this.setState({user: data.user, additionalInfo: data.additionalUserInfo});
            }).catch(err => {
                alert(JSON.stringify(err));
            })
        } else {
            //sign up
            firebase.auth().createUserAndRetrieveDataWithEmailAndPassword(
                this.state.email,
                this.state.password)
            .then(data => {
                this.setState({user: data.user, additionalInfo: data.additionalUserInfo});
                alert('user created');
            }).catch(err => {
                alert(JSON.stringify(err));
            });
        }
    }
    _switchForm = () => {
        this.setState({isRegistered : !this.state.isRegistered});
    }
    render() {
        return (
            <ScrollView style={{flex: 1}}>
                <Text>My app test</Text>
                <TextInput 
                    placeholder="email"
                    onChangeText = {(text) => this.setState({email: text})}
                />

                <TextInput 
                    placeholder="password"
                    // secureTextEntry = {true}
                    onChangeText = {(text) => this.setState({password: text})}
                />

                <View style={{margin: 10}}>
                    <Button title={this.state.isRegistered ? 'Sign In' : 'Sign Up'} onPress={this._singInOrLoginIn} />
                </View>
                <View style={{margin: 10}}>
                    <Button style={{margin: 10}} title={this.state.isRegistered ? 'Back to Sign Up' : 'Sign In'} onPress={this._switchForm} />
                </View>

                <Text>{JSON.stringify(this.state, "", 1)}</Text>
            </ScrollView>
        );
    }
}


