import React, {Component} from "react";
import {Text, View, StyleSheet, Image} from 'react-native';
import CatIcons from "../IconList";
import Color from "react-native-material-color";

/*
props
walletName,
walletType,
icon,
inflow,
outflow
*/


export default class WalletHomeCard extends Component {
    render() {
        const {walletName, walletType, inflow, outflow, icon}  = this.props;
        return  (
            <View style={styles.walletCard}>
                <View style={styles.left}>
                    <Image source={CatIcons.getIcon(icon)} style={{ marginRight: 16, width: 40, height: 40 }} />
                    <View>
                        <Text style={{fontSize: 16, color: Color.GREY[700]}}>{walletName.slice(0, 1).toUpperCase() + walletName.slice(1)}</Text>
                        <Text style={{fontSize: 12, color: Color.GREY[500]}}>{walletType}</Text>
                    </View>
                </View>
                <View style={styles.right}>

                    {/* <Text style={[styles.amt, {fontSize: 16, color: Color.PINK['400'], fontStyle: 'italic'}]}>Spend: {parseFloat(item.walletTotal).toFixed(2)}</Text>
                    <Text style={[styles.amt, {fontSize: 12, color: Color.LIGHTGREEN['700'], fontStyle: 'italic'}]}>Income: {parseFloat(item.walletTotal).toFixed(2)}</Text> */}
                    <Text style={[styles.amt, {fontSize: 12, color: Color.GREY['500'], fontStyle: 'italic'}]}>Inflow: {parseFloat(inflow).toFixed(2)}</Text>
                    <Text style={[styles.amt, {fontSize: 12, color: Color.GREY['700'], fontStyle: 'italic'}]}>Outflow: {parseFloat(outflow).toFixed(2)}</Text>
                    <Text style={[styles.amt, {fontSize: 16, color: Color.LIGHTGREEN['700'], fontStyle: 'italic', }]}>Balance: {parseFloat(inflow - outflow).toFixed(2)}</Text>
                </View>
            </View>
        );
    }
}

WalletHomeCard.defaultProps = {
    walletName: 'Unknown',
    icon: 'wallet',
    walletType: 'Wallets',
    inflow: 0,
    outflow: 0,
}

const styles = StyleSheet.create({
    walletCard: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        paddingVertical: 4,
        paddingHorizontal: 18,
        // borderBottomWidth: 1,
        borderTopWidth: .7,
        borderTopColor: Color.GREY[300],
    },
    left: {
        alignItems: 'center',
        flexDirection: 'row',
    },
    right: {
        alignItems: 'flex-end',
    },
    amt: {
        textAlign: 'right',
    }
});