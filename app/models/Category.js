import DBHelper from "./DBHelper";
import Config from "../Config";
import {CategorySchema} from "./Schema";

export default class Category {

    static getCategories(callback) {
        DBHelper.open(realm => {
            data = realm.objects(CategorySchema.name);
            callback(data);
        });
    }

    static getCategoriesByType(type, callback) {
        DBHelper.open(realm => {
            data = realm.objects(CategorySchema.name).filtered('type == $0', type)
            callback(data);
        });
    }

    static setupInitialCategory(callback) {
        DBHelper.open(realm => {
            var d  = realm.objects('Category');
            if (d.length > 0) {
                return 0;
            }
            realm.write(() => {
                const data = Config.defaultCategories;
                data.map(item => {
                    realm.create('Category', {
                        id: item.id,
                        name: item.name,
                        icon: item.icon,
                        type: item.type,
                    });
                })
                callback();
            });
        });
    }
}