import React, {Component} from "react";
import {Text, View, StyleSheet, Image, TouchableOpacity} from "react-native";
import Color from "react-native-material-color";
import CatIcons from "../IconList";


export default class TopSpendingCard extends Component {
    
    render() {
        const item = this.props.item;
        const percentage = Math.round(item.expense * 100 / this.props.gtotal);
        return (
            <TouchableOpacity style={styles.container}
                activeOpacity={.7}
                onPress={this.props.onPress}
             >
                <Image  source={CatIcons.getIcon(item.icon)} style={{width: 40, height: 40}}></Image>
                <View style={styles.mid}>
                    <Text style={styles.cat}>{item.name}</Text>
                    <View style={[styles.pcontainer, {width: percentage + '%'}]}></View>
                </View>
                <View style={styles.right}>
                    <Text style={styles.amt}>₹ {item.expense}</Text>
                    <Text style={styles.percentage}>{percentage}%</Text>
                </View>
            </TouchableOpacity>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        paddingHorizontal: 18,
        paddingVertical: 12,

        flexDirection: 'row',
        alignItems: 'center',
        borderBottomWidth: 1,
        borderColor: Color.GREY[200],
        backgroundColor: 'white',
    },
    mid: {
        flex: 1,
        marginHorizontal: 16,
        justifyContent: 'center',
    },
    right: {
        justifyContent: 'space-between',
        // backgroundColor: 'red',
    },

    cat: {
        fontSize: 18,
        color: Color.GREY[800],
    },
    pcontainer: {
        // flex: 1,
        height: 8,
        width: '0%',
        borderRadius: 20,
        // borderWidth: .7,
        // borderColor: Color.GREY[800],
        backgroundColor: Color.Green,
        marginTop: 8,
    },
    amt: {
        fontSize: 18,
        color: Color.GREY[800],
        textAlign: 'right',
    },
    percentage: {
        fontSize: 14,
        color: Color.GREY[400],
        textAlign: 'right',
    }
});