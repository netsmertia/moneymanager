import React, {Component} from "react";
import {View, Text, StyleSheet, Image, TouchableOpacity, ScrollView} from "react-native";
import IconList from "../../IconList";
import Color from "react-native-material-color";
import WalletCard from "../../components/WalletHomeCard";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import {Wallet, Transaction} from "../../models";

export default class Wallets extends Component {
    constructor(props) {
        super(props);
        this.state = {
            wallets: {online: [], cash: [], bank: []},
        }
    }
    static navigationOptions = ({navigation}) => {
        const {onAddWallet}  = navigation.state.params || {};
        return (
            {
                headerRight: <TouchableOpacity style={styles.addBtn} onPress={onAddWallet}>
                                <Icon name="plus-circle" size={20}/>
                                <Text style={{paddingLeft: 4, fontSize: 14,}}>Add Wallet</Text>
                            </TouchableOpacity>
            }
        )
        /* No more header config here! */
      };
    

    componentWillMount() {
        this.props.navigation.setParams({onAddWallet: this._onAddWallet});
        Wallet.getWalletWiseTotal((new Date()).toISOString(), data => {
            this.setState({wallets: this._groupByWalletType(data)});
        });
    }

    _onAddWallet  = () => {
        alert('add button clicked');
    }
    _groupByWalletType = (data) => {
        ndata = Object.values(data).reduce((r, i) =>  {
            key = i.walletType;
            r[key] = r[key]  || [];
            r[key].push(i);
            return r;
        }, {});
        return ndata;
    }
    _renderWallets = (wallets) => {
        return wallets.map(item => {
            return <WalletCard key = {item.id}  />
        });
    }

   render () {
        return (
            <ScrollView style = {styles.container}>
                {/* <Text>{JSON.stringify(this.state,'', 1)}</Text> */}
                
                <View style={styles.section}>
                    <View style={styles.header}>
                        <Text style={styles.sectionTitle}>Cash Wallets</Text> 
                    </View>
                    { this._renderWallets(this.state.wallets.cash)}
                </View>

                <View style={styles.section}>
                    <View style={styles.header}>
                        <Text style={styles.sectionTitle}>Online Wallets</Text> 
                    </View>
                    { this._renderWallets(this.state.wallets.online)}
                </View>

                <View style={styles.section}>
                    <View style={styles.header}>
                        <Text style={styles.sectionTitle}>Bank Wallets</Text> 
                    </View>
                    {
                        this.state.wallets.online.map(item => {
                            return <WalletCard key = {item.id} />
                        })
                    }
                    {/* {this._renderFooter('Add New Wallet', this._onAddBankWallet)} */}
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    section: {
        elevation: 4,
        margin: 10,
        backgroundColor: 'white',
    },
    header: {
        paddingHorizontal: 18,
        paddingVertical: 8,
        backgroundColor: Color.GREY[300],
    },
    sectionTitle: {
        fontSize: 18,
        // color: 'white',
    },

    sectionFooter: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        backgroundColor: Color.GREY[200],
        width: '100%',
        paddingHorizontal: 18,
        paddingVertical: 8,
    },

    addBtn: {
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        // backgroundColor: Color.GREEN[100],
        backgroundColor: 'white',
        borderRadius: 40,
        paddingHorizontal: 4,
        paddingVertical: 2,
        paddingRight: 10,
        marginRight: 18,
    },

});