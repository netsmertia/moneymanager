/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
import React, {Component} from "react";
import {Text, View} from "react-native";
import {StackNavigator, DrawerNavigator, TabNavigator, TabBarTop} from "react-navigation";
import Color from "react-native-material-color";
import {
    HomeScreen, 
    AddTransactionScreen,
    EditTransaction,
    AllTransactionsScreen,
    CategorySpendingsScreen,
    AllCategoriesSpendingsScreen,
    Seeder,
    TestScreen,
} from "./app/screens";
import {Wallets} from "./app/screens/wallet";
import CategoryChooserTab from "./app/screens/CategoryChooserTabs";
import {ExpenseCategoriesTab, IncomeCategoriesTab} from "./app/screens/CategorySpendingTabs";

const CategorySpendingTabs = TabNavigator({
  ExpenseCategories: {
    screen: ExpenseCategoriesTab,
    navigationOptions: {
      title: 'Expense',
    },
  },
  IncomeCategories: {
    screen: IncomeCategoriesTab,
    navigationOptions: {
      title: 'Income',
    },
  }
}, {
    initialRouteName: 'ExpenseCategories',
    backBehavior: 'none',
    tabBarComponent: TabBarTop,
    tabBarOptions: {
      style: {
        backgroundColor: Color.GREEN[700],
      },
      indicatorStyle: {
        backgroundColor: Color.LIGHTGREEN['100'],
      }
    }
});

const MainStack = StackNavigator({
  Home: {
    screen: HomeScreen,
    navigationOptions: {
      title: 'Money Manager',
    }
  },
  AddTransaction: {
    screen: AddTransactionScreen,
    navigationOptions: {
      title: 'Add Transaction',
    }
  },
  CategoryChooser:  {
    screen: CategoryChooserTab,
    navigationOptions: {
      title: 'Select  Category',
      headerStyle: {
        elevation: 0,
        backgroundColor: Color.GREEN[700],
      }
    }
  },
  EditTransaction: {

    screen: EditTransaction,
    navigationOptions: {
      title: 'Edit Transaction',
    }
  },
  AllTransactions: {
    screen: AllTransactionsScreen,
    navigationOptions: {
      title: 'Transactions',
    }
  },
  CategorySpendings: {
    screen: CategorySpendingsScreen,
    navigationOptions: {
      title: 'Category Transactions',
    }
  },
  AllCategoriesSpendings: {
      screen: CategorySpendingTabs,
      navigationOptions: {
        title: 'All Categories',
        headerStyle: {
          elevation: 0,
          backgroundColor: Color.GREEN[700],
        },
      }
  }, 
  Wallets: {
    screen: Wallets,
    navigationOptions: {
      title: 'Wallets',
    }
  },

  Test: {
    screen: TestScreen,
    navigationOptions: {
      title: 'Test Screen',
    }
  }

}, {
  initialRouteName: 'Wallets',
  navigationOptions: {
    headerStyle: {
      backgroundColor: Color.GREEN[700],
    },
    headerTitleStyle: {
      color: 'white',
    },
    headerTintColor: 'white',
  }
});

const RootStack = DrawerNavigator({
  MainStack: MainStack,
  Seeder: Seeder,
});

export default class App extends Component{
  render() {
    return (
      <RootStack />
    );
  }
}