
const WalletSchema = {
    name: 'Wallet',
    primaryKey: 'id',
    properties: {
        id: 'string',
        name: 'string',
        type: 'string',
        target: {type: 'int', default: 0},
        icon: 'string',
    }
}

const CategorySchema = {
    name: 'Category',
    primaryKey: 'id',
    properties: {
        id: 'int',
        name: 'string',
        icon: 'string',
        parentId: {type: 'int', default: 0},
        deleted: {type: 'bool', default: false},
        type: {type: 'string', default: 'expense'}
    },
};

const TransactionSchema = {
    name: 'Transaction',
    primaryKey: 'id',
    properties: {
        id: 'string',
        title: 'string?',
        category: 'Category',
        amt: 'double',
        date: 'date',
        createdAt: 'date',
        deleted: {type: 'bool', default: false},
        wallet: {type: 'Wallet'},
        toWallet: {type: 'Wallet?'},
        excludeFromTotal: {type: 'bool', default: false},
        type: {type: 'string', default: 'expense'},
    }
}

export {CategorySchema, TransactionSchema, WalletSchema};