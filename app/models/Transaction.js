import DBHelper from "./DBHelper";
import Config from "../Config";
import {TransactionSchema, CategorySchema, WalletSchema} from "./Schema";
import Utils from "../Utils";
import moment from "moment";
import Wallet from "./Wallet";

export default class Transaction {



    static addTransaction(data, callback) {
        DBHelper.open(realm => {
            realm.write(() => {
                t = realm.create('Transaction', {...data,  id: Utils.uuid(), date: moment(data.date).toISOString()});
                callback();
            });
        });
    } 

    static updateTransaction(transaction, callback) {
        DBHelper.open(realm => {
            realm.write(() => {
                realm.create('Transaction', transaction, true);
            });
            callback();
        })
    }

    static getTransactionById = (id, callback) => {

        DBHelper.open((realm) => {
            data = realm.objects('Transaction').filtered('id == $0', id)[0];
            callback(data);
        });
    }

    static getRecentTransactions = (callback) => {
        DBHelper.open((realm) => {
            data = realm.objects('Transaction').filtered('deleted == false').sorted('createdAt', true).slice(0, 5);
            callback(data);
        });
    } 

    static getAllTransactions = (duration, category = undefined, callback) => {
        DBHelper.open((realm) => {
            const {startDate, endDate} = Utils.getMonthBound(duration);

            data = realm.objects('Transaction').filtered('deleted == false and date >= $0 and date <= $1', startDate, endDate).sorted('date', true);
            if (category) {
               data = data.filtered('category.name == $0', category.name);
            }
            data = data.reduce((r, c) => {
                const date = moment(c.date).startOf('day').toISOString();
                r[date] = r[date] || [];
                r[date].push(c);
                    return r;
                }, {});
                ndata = Object.keys(data).map(index => {
                    total = data[index].reduce((r, c) => {
                            if (Config.transactionType.expense == c.type) {
                                r.outflow += c.amt;
                            } else if (Config.transactionType.income == c.type) {
                                r.inflow += c.amt;
                            }
                        return r;
                    }, {inflow: 0, outflow: 0});
                    return {
                            header: {
                                title: index,
                                total: total,
                            },
                            data: data[index]
                        };
            });
            callback(ndata);
        });
    }

    static getTotalSpending(date, callback) {
        DBHelper.open(realm => {
            const {startDate, endDate} = Utils.getMonthBound(date);
            data = Math.round(realm.objects('Transaction').filtered('deleted == false and date >= $0 and date <= $1', startDate, endDate).sum('amt'), 0);
            callback(data);
        })
    }
    static getCategoryWiseSpendings = (date, callback) => {
        DBHelper.open((realm) => {
            const {startDate, endDate} = Utils.getMonthBound(date);
            data = realm.objects('Transaction').filtered('deleted == false and date >= $0 and date <= $1 and type == $2', startDate, endDate, 'expense');

            //groupby category and add total of transaction;
            topSpendings = data.reduce((r, item) => {
                var cat = item.category.name;
                r[cat] = r[cat] || {
                    id: item.category.id,
                    name: cat,
                    expense: 0,
                    income: 0,
                    icon: item.category.icon,
                };
                r[cat].expense += item.amt;
                return r;
            }, {});

            //sort top spending according to total amt.
            topSpendings = Object.values(topSpendings).sort((a, b) => b.total - a.total);
            callback(topSpendings);
        });
    }

    static getCategoryWiseIncome = (date, callback) => {
        DBHelper.open((realm) => {
            const {startDate, endDate} = Utils.getMonthBound(date);
            data = realm.objects('Transaction').filtered('deleted == false and date >= $0 and date <= $1 and type == $2', startDate, endDate, 'income');

            //groupby category and add total of transaction;
            processed = data.reduce((r, item) => {
                var cat = item.category.name;
                r[cat] = r[cat] || {
                    id: item.category.id,
                    name: cat,
                    expense: 0,
                    income: 0,
                    icon: item.category.icon,
                };
                r[cat].income += item.amt;
                return r;
            }, {});

            //sort top spending according to total amt.
            processed = Object.values(processed).sort((a, b) => b.total - a.total);
            callback(processed);
        });
    }


    static seed(callback) {
        DBHelper.open(realm => {
            realm.write(() => {
                var cat = realm.objects(CategorySchema.name);
                var wallet = realm.objects(WalletSchema.name);
                // alert(JSON.stringify(wallet));
                if (cat.length == 0 || wallet.length == 0) {
                    alert('no wallet or category');
                    return 0;
                }
                let trans = realm.objects(TransactionSchema.name);
                realm.delete(trans);
                for (var i = 0; i < 2000; i++) {
                    realm.create(TransactionSchema.name, {
                        id: Utils.uuid(),
                        title: Utils.getTitle(),
                        category: cat[Utils.random(0, cat.length - 1)],
                        amt: Utils.random(100, 1000),
                        date: moment([2018, Utils.random(0, 4), Utils.random(1, 28)]).toISOString(),
                        createdAt: (new Date()).toISOString(),
                        wallet: wallet[Utils.random(0, wallet.length - 1)],
                    });
                }
                callback();
            })
        });
    }
}