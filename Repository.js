import {Category, Transaction, Wallet} from "./Models";
import Realm, { schemaVersion } from "realm";
import Utils from "./Utils";
import moment from "moment";

export default class Repository {
    static open = (callback) => {
        Realm.open({
            schema: [Category, Transaction, Wallet],
            schemaVersion: 13,
            migration: (oldRealm, newRealm) => {
                newRealm.deleteAll();
            }
        })
        .then(realm => {
            callback(realm);
        });
    }

    //Transactions related
    static getRecentTransactions = (callback) => {
        Repository.open((realm) => {
            // const startDate = (new Date(2018, 2,31)).toISOString();
            // const endDate = (new Date()).toISOString();
            data = realm.objects('Transaction').filtered('deleted == false').sorted('createdAt', true).slice(0, 5);
            callback(data);
        });
    }

    
    static getAllTransactions = (duration, category = undefined, callback) => {
        Repository.open((realm) => {
            const {startDate, endDate} = Utils.getMonthBound(duration);

            data = realm.objects('Transaction').filtered('deleted == false and date >= $0 and date <= $1', startDate, endDate);
            if (category) {
               data = data.filtered('category.categoryName == $0', category.categoryName);
            }
            data = data.reduce((r, c) => {
                const date = moment(c.date).format('DD MMM YYYY');
                r[date] = r[date] || [];
                r[date].push(c);
                    return r;
                }, {});
                ndata = Object.keys(data).map(index => {
                    total = data[index].reduce((r, c) => {
                        r = r + c.amt;
                        return r;
                    }, 0);
                    return {
                            header: {
                                title: index,
                                total: parseFloat(total).toFixed(2),
                            },
                            data: data[index]
                        };
            });
            callback(ndata);
        });
    }
    static getTransactionById = (id, callback) => {

        Repository.open((realm) => {
            data = realm.objects('Transaction').filtered('id == $0', id)[0];
            callback(data);
        });
    }
    //done
    static getCategoryWiseSpendings = (date, callback) => {
        Repository.open((realm) => {

            const {startDate, endDate} = Utils.getMonthBound(date);
            data = realm.objects('Transaction').filtered('deleted == false and date >= $0 and date <= $1', startDate, endDate);

            //groupby category and add total of transaction;
            topSpendings = data.reduce((r, item) => {
                var cat = item.category.categoryName;
                r[cat] = r[cat] || {
                    id: item.category.id,
                    categoryName: cat,
                    total: 0,
                    icon: item.category.iconFile,
                };
                r[cat].total += item.amt;
                return r;
            }, {});

            //sort top spending according to total amt.
            topSpendings = Object.values(topSpendings).sort((a, b) => b.total - a.total);
            callback(topSpendings);
        });
    }

    static getWalletWiseTotal = (date,callback) => {
        Repository.open(realm => {
            const {startDate, endDate} = Utils.getMonthBound(date);
            rows = realm.objects('Transaction').filtered('deleted == false and date >= $0 and date <= $1', startDate, endDate);
            data = rows.reduce((r, item) =>{
                var wallet = item.wallet.walletName;
                r[wallet] = r[wallet] || {
                    id: item.wallet.id,
                    walletName: item.wallet.walletName,
                    walletIcon: item.wallet.walletIcon,
                    walletTye: item.wallet.walletType,
                    walletTotal: 0,
                };
                r[wallet].walletTotal += item.amt;
                return r;
            }, {});
            console.log(data);
            callback(data);
        });
    }
    static addTransaction(data, callback) {
        Repository.open(realm => {
            realm.write(() => {
                t = realm.create('Transaction', {...data, id: Utils.uuid(), date: moment(data.date).toISOString()});
                console.log('transaction added');
                console.log(t);
                callback();
            });
        });
    } 
    static updateTransaction(transaction, callback) {
        Repository.open(realm => {
            realm.write(() => {
                realm.create('Transaction', transaction, true);
            });
            callback();
        })
    }
    static deleteTransaction(transaction, callback) {
        Repository.open(realm => {
            realm.write(() => {
                realm.create('Transaction', {...transaction, deleted: true}, true);
                callback();
            });
        });
    }

    static getTotalSpend(date, callback) {
        Repository.open(realm => {
            const {startDate, endDate} = Utils.getMonthBound(date);
            data = Math.round(realm.objects('Transaction').filtered('deleted == false and date >= $0 and date <= $1', startDate, endDate).sum('amt'), 0);
            callback(data);
        })
    }

    //done
    static getCategories(callback) {
        Repository.open(realm => {
            data = realm.objects('Category');
            callback(data);
        });
    }
    

    //done
    static dummyCategories(callback) {
        Repository.open(realm => {
            
            var d  = realm.objects('Category');
            if (d.length > 0) {
                return 0;
            }
            realm.write(() => {

                    const data = [
                            {id: 1, name: 'Bills', icon: 'bills' },
                            {id: 2, name: 'Shopping', icon: 'shopping'},
                            {id: 3, name: 'Household', icon: 'bucket'},
                            {id: 4, name: 'Groceries', icon: 'groceries'},
                            {id: 5, name: 'Gifts', icon: 'gift2'},
                            {id: 6, name: 'Laundary', icon: 'shirt'},
                            {id: 7, name: 'Saving', icon: 'saving'},
                            {id: 8, name: 'Family', icon: 'user'},
                            {id: 9, name: 'Cash', icon: 'wallet'},
                            {id: 10, name: 'Service', icon: 'service'},
                            {id: 11, name: 'Studies', icon: 'library'},
                            {id: 12, name: 'Card', icon: 'creditcard'},
                            {id: 13, name: 'Electronics', icon: 'camera'},
                            {id: 14, name: 'Home', icon: 'house'},

                        ];
                data.map(item => {
                    realm.create('Category', {
                        id: item.id,
                        categoryName: item.name,
                        iconFile: item.icon,
                    });
                })
                callback();
            });
        });
    }

    static dummyTransactions(callback) {
        Repository.open(realm => {
            realm.write(() => {
                var cat = realm.objects('Category');
                var wallet = realm.objects('Wallet');
                if (cat.length == 0 || wallet.length == 0) {
                    alert('no wallet or category');
                    return 0;
                }
                let trans = realm.objects('Transaction');
                realm.delete(trans);
                for (var i = 0; i < 2000; i++) {
                    realm.create('Transaction', {
                        id: Utils.uuid(),
                        title: Utils.getTitle(),
                        category: cat[Utils.random(0, cat.length - 1)],
                        amt: Utils.random(100, 1000),
                        date: moment([2018, Utils.random(0, 4), Utils.random(1, 28)]).toISOString(),
                        createdAt: (new Date()).toISOString(),
                        wallet: wallet[Utils.random(0, wallet.length - 1)],
                    })
                }
                callback();
            })
        });
    }

    static createWallet(data, callback) {
        Repository.open(realm => {
            realm.write(() => {
                realm.create('Wallet', data);
                callback();
            });
        });
    }

    //done
    static setupInitialWallet = () => {
        Repository.open(realm => {
            realm.write(() => {
                const wallet = realm.objects('Wallet');
                if ( wallet.length > 0) {
                    return 0;
                } else {
                    realm.create('Wallet', {
                        id: Utils.uuid(),
                        walletName: 'Cash Wallet',
                        walletIcon: 'wallet',
                        walletType: 'cash',
                    });

                    realm.create('Wallet', {
                        id: Utils.uuid(),
                        walletName: 'Online Wallet',
                        walletIcon: 'creditcard',
                        walletType: 'online',
                    });
                }
            })
        }) 
    }
    
}