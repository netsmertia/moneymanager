import React, {Component} from "react";
import {Text, View, StyleSheet, TouchableOpacity, SectionList} from "react-native";
import Utils from "../Utils";
import Color from "react-native-material-color";
import TCard from "../components/TransactionCard";
import moment from "moment";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import sectionListGetItemLayout from "react-native-section-list-get-item-layout";
import {Transaction, } from "../models";
import SectionListHeader from "../components/SectionListHeader";

export default class CategorySpendings extends Component {

    constructor(props) {
        super(props);
        this.state = {
            duration: (new Date()).toISOString(),
            category: [],
            trans: [],
        }

        this.getItemLayout = sectionListGetItemLayout({
            getItemHeight : () => 65,
            getSectionHeaderHeight : () => 30,
        });
    }

    componentWillMount() {
        var {category} = this.props.navigation.state.params;
        console.log(this.props);
        this.setState({category: category});
        Transaction.getAllTransactions(this.state.duration, category, data => {
            this.setState({trans: data});
            console.log(data);
        });
    }

    _renderItem = ({item}) => {
        return (
            <TCard
                data = {item}
            ></TCard>
        )
    }
    render() {
        return (
            <View style={styles.container}>
                { Utils.renderStatusBar() }
                {/* <Text>{JSON.stringify(this.state.category)}</Text> */}

                <SectionList
                    keyExtractor = {(item, index) => 'key-' + index}
                    style={styles.sectionList}
                    sections = {this.state.trans}
                    renderItem = {this._renderItem}
                    renderSectionHeader= {({section}) => <SectionListHeader section={section} />}
                    stickySectionHeadersEnabled={true}
                    getItemLayout = {this.getItemLayout}
                    initialNumToRender={10}
                ></SectionList>
            </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
    },

    sectionHeader: {
        paddingHorizontal: 18,
        paddingVertical: 8,
        backgroundColor: Color.GREY[300],
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
});