import React, {Component} from "react";
import ExpenseCategoryTab from "./ExpenseCategoryTab";
import IncomeCategoryTab from "./IncomeCategoryTab"
import WalletChooserTab from "./WalletChooserTab";

import {TabNavigator} from "react-navigation";
import Color from "react-native-material-color";


export default class CategoryChooserTab extends Component {

    componentWillMount () {
        const {defaultTab} = this.props.navigation.state.params;
        if (defaultTab == undefined) {
            defaultStatus = 'ExpenseCategoryTab';
        }
        CategoryChooseStack =  TabNavigator({
        IncomeCategoryTab: {
            screen: IncomeCategoryTab,
            navigationOptions: {
            title: 'Income',
            }
        },
        ExpenseCategoryTab: {
            screen: ExpenseCategoryTab,
            navigationOptions: {
            title: 'Expense',
            }
        },
        TransferTab: {
            screen: WalletChooserTab,
            navigationOptions: {
            title: 'Transfer',
            }
        },
        }, {
        
            initialRouteName: defaultTab,
            backBehavior: 'none',
            navigationOptions: {
            style: {
                header: {
                elevation: 0,
                }
            },

            },
            tabBarOptions: {
                style: {
                    backgroundColor: Color.GREEN[700],
                },
                indicatorStyle: {
                    backgroundColor: Color.LIGHTGREEN['100'],
                }
            }
        });
    }

    render() {
        return <CategoryChooseStack screenProps={{...this.props.navigation.state.params, pn: this.props.navigation}}/>
    }
}
