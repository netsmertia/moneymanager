import React, {Component} from "react";
import {Text, View, StyleSheet, Image, TouchableOpacity} from "react-native";
import Color from "react-native-material-color";
import CatIcons from "../IconList";
import PropTypes from "prop-types";

export default class CategoryProgressCard extends Component {
    
    render() {
        const {category, gtotal, value, icon} = this.props;
        const percentage = Math.round(value * 100 / gtotal);
        return (
            <TouchableOpacity style={styles.container}
                activeOpacity={.7}
                onPress={this.props.onPress}
             >
                <Image  source={CatIcons.getIcon(icon)} style={{width: 40, height: 40}}></Image>
                <View style={styles.mid}>
                    <Text style={styles.cat}>{category}</Text>
                    <View style={[styles.pcontainer, {width: percentage + '%'}]}></View>
                </View>
                <View style={styles.right}>
                    <Text style={styles.amt}>₹ {value}</Text>
                    <Text style={styles.percentage}>{percentage}%</Text>
                </View>
            </TouchableOpacity>
        );
    }
}
CategoryProgressCard.propTypes = {
    gtotal: PropTypes.number.isRequired,
    value: PropTypes.number.isRequired,
    category: PropTypes.string.isRequired,
    icon: PropTypes.string.isRequired,
    onPress: PropTypes.func,
}

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: 18,
        paddingVertical: 12,

        flexDirection: 'row',
        alignItems: 'center',
        borderBottomWidth: 1,
        borderColor: Color.GREY[200],
        backgroundColor: 'white',
    },
    mid: {
        flex: 1,
        marginHorizontal: 16,
        justifyContent: 'center',
    },
    right: {
        justifyContent: 'space-between',
        // backgroundColor: 'red',
    },

    cat: {
        fontSize: 18,
        color: Color.GREY[800],
    },
    pcontainer: {
        // flex: 1,
        height: 8,
        width: '0%',
        borderRadius: 20,
        // borderWidth: .7,
        // borderColor: Color.GREY[800],
        backgroundColor: Color.Green,
        marginTop: 8,
    },
    amt: {
        fontSize: 18,
        color: Color.GREY[800],
        textAlign: 'right',
    },
    percentage: {
        fontSize: 14,
        color: Color.GREY[400],
        textAlign: 'right',
    }
});