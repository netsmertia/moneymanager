export default class CatIcons {
    static list = {
        bill : require('./img/bill.png'),
        bills : require('./img/bills.png'),
        box : require('./img/box.png'),
        box2 : require('./img/box2.png'),
        box3 : require('./img/box3.png'),
        bucket : require('./img/bucket.png'),
        camera : require('./img/camera.png'),
        clipboard : require('./img/clipboard.png'),
        cosmetics : require('./img/cosmetics.png'),
        creditcard : require('./img/creditcard.png'),
        email : require('./img/email.png'),
        game : require('./img/game.png'),
        gift : require('./img/gift.png'),
        gift2 : require('./img/gift2.png'),
        groceries : require('./img/groceries.png'),
        house : require('./img/house.png'),
        library : require('./img/library.png'),
        money : require('./img/money.png'),
        moneybag : require('./img/moneybag.png'),
        music : require('./img/music.png'),
        saving : require('./img/saving.png'),
        service : require('./img/service.png'),
        shirt : require('./img/shirt.png'),
        shoes : require('./img/shoes.png'),
        shop : require('./img/shop.png'),
        shop2 : require('./img/shop2.png'),
        shopping : require('./img/shopping.png'),
        shopping2 : require('./img/shopping2.png'),
        user : require('./img/user.png'),
        wallet : require('./img/wallet.png'),
        wallet2 : require('./img/wallet2.png'),
    };


    static getIcon = (key) => {
        if (CatIcons.list.hasOwnProperty(key)) {
            return CatIcons.list[key];
        }
    }
}
