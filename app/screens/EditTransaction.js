
import React, {Component} from "react";
import {Text, View, StyleSheet, TextInput, Image, TouchableOpacity, DatePickerAndroid, Keyboard, ScrollView} from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import Color from "react-native-material-color";
import moment from "moment";
import CatIcons from "../IconList";

import {Category, Transaction, Wallet} from "../models";
import WalletSelectorModal from "../components/WalletSelectorModal";
import Utils from "../Utils";
import Config from "../Config";
import {CategoryChooseStack} from "../../App";
export default class EditTransaction extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: 'natwar sing',
            transaction: {},
            walletList: {},
            isModalVisible: false,
            isToWalletModalVisible: false,
        }
    }
    static navigationOptions = ({navigation}) => {
        const params = navigation.state.params || {};
        return {
            headerRight: <TouchableOpacity onPress={params.onSave}><Text style={{color: 'white', padding: 16, fontSize: 16,}}>Save</Text></TouchableOpacity>
        }
    };


    componentWillMount() {
        const {transaction} = this.props.navigation.state.params;
        this.setState({transaction: transaction});
        this.props.navigation.setParams({onSave: this._onSave});
        // const { category, txType, toWallet } = this.props.screenProps;
        // this.setState({category: category, txType: txType});
        Wallet.getAllWallets(data => {
            this.setState({walletList: data});
            // if (toWallet && Object.keys(data).length > 1) {
            //     fromWallets = Object.values(data).filter(item => item.id != toWallet.id);
            //     console.log(fromWallets);
            //     this.setState({wallet: fromWallets[0]});
            //     this.setState({toWallet: toWallet});
            // } else {
            //     this.setState({wallet: data[0]});
            // }
            // this.setState({wallet: wallet ? wallet : data[0]});
        });
    }

    _onSave = () => {
        if (this.state.transaction.amt == undefined || this.state.transaction.amt.toString().split('').length == 0) {
            alert('Please enter amount' + this.state.transaction.amt);
            return;
        };

        if (Object.keys(this.state.transaction.category).length == 0) {
            alert('Please select a category');
            return;
        }

        if (Object.keys(this.state.transaction.wallet).length == 0) {
            const type = this.state.txType == Config.transactionType.income ? 'To' : 'From';
            alert('Please select ' + type + ' Wallet');
            return;
        }
        if (this.state.transaction.type == Config.transactionType.transfer 
                && Object.keys(this.state.transaction.toWallet).length == 0) {
            alert('Please select To Wallet');
            return;
        }

        this.setState({pageCall: 'HomePage'});
        if (this.state.pageCall != 'HomePae') {
            Transaction.updateTransaction({
                ...this.state.transaction,
                amt: Utils.toAmt(this.state.transaction.amt),
                toWallet: this.state.transaction.toWallet == null || 
                            Object.keys(this.state.transaction.toWallet).length == 0 ?
                            undefined : this.state.transaction.toWallet,
            }, () => {
                this.props.navigation.goBack(null);
            });
        }
    }
    _selectCateogry = () => {
        Keyboard.dismiss();
        var defaultTab = '';
        switch(this.state.transaction.type) {
            case Config.transactionType.income: 
                defaultTab = 'IncomeCategoryTab';
                break;

            case Config.transactionType.expense: 
                defaultTab = 'ExpenseCategoryTab';
                break;

            case Config.transactionType.transfer: 
                defaultTab = 'TransferTab';
        }

        this.props.navigation.navigate({key: 'categoryChooser', routeName: 'CategoryChooser', params: {updateSelectedCategory: this._updateSelectedCategory, defaultTab: defaultTab, updateToWallet: this._onToWalletSelect}});
    }
     _selectDate = async () => {
        try {
            const {action, year, month, day}  = await DatePickerAndroid.open({
                date: new Date(),
            });
            if (action !== DatePickerAndroid.dismissedAction) {
                transaction = this.state.transaction;
                this.setState({transaction: {...transaction, date: new Date(year, month, day).toISOString()}});
            }
        } catch({code, message}) {
            alert('some thing went worng ' + message);
        }
    }
    _selectWallet = () => {
        this.setState({isModalVisible: true});
    }

    _selectToWallet = () => {
        this.setState({isToWalletModalVisible: true});
    }
    _onWalletSelect = (item) => {
        const {transaction} = this.state;
        if (item.id == transaction.wallet.id) {
            console.log('return');
            return;
        } 
        if (transaction.type == Config.transactionType.transfer 
                    && Object.keys(this.state.walletList).length > 1 ) {
            const walletList = Object.values(this.state.walletList).filter(i => i.id != item.id);
            this.setState({transaction: {...transaction, toWallet: walletList[0], wallet: item}});
            return;
        } else {
            this.setState({transaction: {...transaction, toWallet: {}, wallet: item}});
        }
    }

    _onToWalletSelect = (item, category) => {
        const {transaction}  = this.state;
        if (transaction.toWallet != undefined && transaction.toWallet.id == item.id) {
            return;
        }
        const walletList = Object.values(this.state.walletList).filter(i => i.id != item.id);
        this.setState({transaction: {...transaction, 
                                        wallet: walletList[0], 
                                        toWallet: item, 
                                        type: Config.transactionType.transfer,
                                        category: category != undefined ? category : transaction.category
                                    }});
    }

    _closeModal = () => {
        this.setState({isModalVisible: false});
    }

    _closeToWalletModal = () => {
        this.setState({isToWalletModalVisible: false});
    }
    _updateSelectedCategory = (item) => {
        const transaction = this.state.transaction;
        const toWallet = item.type == Config.categoryType.transfer ? transaction.toWallet : {};
        this.setState({transaction: {...transaction, category: item, type: item.type, toWallet: toWallet }});
    }

    _renderCatIcon = () => {
        // iconFile = '../img/shopping.png';
        const {category} = this.state.transaction;
        iconFile = category != undefined ? category.icon : undefined;
        if (iconFile == undefined) {
            return <Icon style={styles.leftIcon} name="help-circle" size={32} color="green"/>
        } else {
            return  <Image source={CatIcons.getIcon(iconFile)}
                style={{width:40, height: 40}}
            ></Image>
        }
    }

    _renderIcon = (icon) => {
        // iconFile = '../img/shopping.png';
        if (icon == undefined) {
            return <Icon style={styles.leftIcon} name="help-circle" size={32} color="green"/>
        } else {
            return  <Image source={CatIcons.getIcon(icon)}
                        style={{width:40, height: 40}} />
        }
    }

    _renderToWallet = (type, item) => {
        if (type == Config.transactionType.transfer && Object.keys(item) != 0) {
            return (
                <TouchableOpacity 
                    onPress={this._selectToWallet}
                    style={[styles.boxRow]}>
                    {this._renderIcon(item.icon)}

                    <View style={styles.captionBox}>
                        <Text style={styles.caption}>{type == 'transfer' ? 'To' : 'From' } Wallet</Text>
                        <Text style={styles.heading}>{Object.keys(item).length == 0 ? 'Select Wallet' : item.name}</Text>
                    </View>
                </TouchableOpacity>
            )
        }
    }
    render() {
        const {category, wallet, toWallet, type, id, title, amt,date} = this.state.transaction;
        return (
            <ScrollView style={styles.container}>
                <View style={styles.amtCard}>
                    <Icon name="currency-inr" size={32} color="green"/>
                    <TextInput
                        style={styles.amtInput}
                        underlineColorAndroid="transparent"
                        placeholder="Enter Amount"
                        keyboardType='numeric'
                        value={this.state.transaction.amt.toString()}
                        onChangeText={(text) => this.setState({transaction: {...this.state.transaction, amt: text}})}
                    ></TextInput>
                </View>

                <View style={{paddingHorizontal: 0}}>

                    <View style={[styles.boxRow]}>
                        <Icon style={styles.leftIcon} name="comment-text" size={32} color="green"/>
                        <TextInput placeholder="Note" 
                            underlineColorAndroid="transparent"
                            value={this.state.transaction.title}
                            onChangeText={(text) => this.setState({transaction: {...this.state.transaction, title: text}})}
                        style={styles.bigText}></TextInput>
                    </View>

                    <TouchableOpacity onPress={this._selectDate} style={[styles.boxRow]}>
                        <Icon style={styles.leftIcon} name="calendar" size={32} color="green" />
                        {/* <Text style={styles.bigText}>{moment(this.state.transaction.date).startOf('day').diff(moment(new Date()).startOf('day'), 'days')}</Text> */}
                        <Text style={styles.bigText}>{moment(date).startOf('day').diff(moment(new Date()).startOf('day'), 'days') == 0 ? 'Today' : moment(date).format('DD MMM YYYY')}</Text>
                    </TouchableOpacity>

                    <TouchableOpacity 
                        onPress={this._selectCateogry}
                        style={[styles.boxRow]}>
                        {this._renderCatIcon()}
                        <View style={styles.captionBox}>
                            <Text style={styles.caption}>Category</Text>
                            <Text style={styles.heading}>{category.name}</Text>
                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity 
                        onPress={this._selectWallet}
                        style={[styles.boxRow]}>
                        {this._renderIcon(wallet.icon)}

                        <View style={styles.captionBox}>
                            <Text style={styles.caption}>{type == 'income' ? 'To' : 'From' } Wallet</Text>
                            <Text style={styles.heading}>{Object.keys(wallet).length == 0 ? 'Select Wallet' : wallet.name}</Text>
                        </View>
                    </TouchableOpacity>
                   
                   {
                       this._renderToWallet(type, toWallet)
                   }
                </View>
                <Text>{JSON.stringify(this.state.transaction, '', 1)}</Text>
                <WalletSelectorModal 
                        walletType = "from"
                        txType = {type}
                        isVisible = {this.state.isModalVisible} 
                        closeModal = {this._closeModal}
                        wallets = {this.state.walletList}
                        onWalletSelect = {this._onWalletSelect}
                        />

                <WalletSelectorModal 
                        walletType = "to"
                        txType = {type}
                        isVisible = {this.state.isToWalletModalVisible} 
                        closeModal = {this._closeToWalletModal}
                        wallets = {this.state.walletList}
                        onWalletSelect = {this._onToWalletSelect}
                        />

            </ScrollView>
        )
    }
}



const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    saveBtn: {
        color: 'white',
    },
    amtCard: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 28,
        backgroundColor: Color.GREY[300],
        // marginTop: 30,
        
    },
    amtInput:{
        width: 200,
        fontSize: 30,
    },
    leftIcon: {
        width: 40,
    },
    bigText: {
        fontSize: 20,
        marginLeft: 18,
        flex: 1,
    },
    boxRow: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        height: 70,
        paddingHorizontal: 18,
        borderBottomWidth: .7,
        borderColor: Color.GREY[400],
    },
    captionBox: {
        marginLeft: 18,
        flex: 1,
    },
    caption: {
        fontSize: 12,
        color: Color.GREY[400],
    },
    heading: {
        fontSize: 20,
    }

});
