import React, {Component } from "react";
import {
    Text, Image, View,Button,  
    Modal,StyleSheet, Dimensions,
    TouchableOpacity, ScrollView,
} from "react-native";
import {Wallet} from "../models";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import CatIcons from "../IconList";
import Color from "react-native-material-color";


export default class WalletSelectorModal extends Component {

    constructor(props) {
        super(props);
        this.state = {
            title : 'From',
        }
    }

    componentWillMount = () => {
        const {walletType, txType} = this.props;
        if (walletType == 'from' && txType == 'expense') {
            this.setState({title: 'From'});
        } else if (walletType == 'from' && txType == 'income') {
            this.setState({title: 'To'});
        } else if (walletType == "to" && txType == 'transfer') {
            this.setState({title: 'To'});
        }
    }
    _onWalletSelect = (item) => {
        this.props.onWalletSelect(item);
        this.props.closeModal();
    }
    _renderIcon = (icon) => {
        if (icon == undefined) {
            return <Icon style={styles.leftIcon} name="help-circle" size={32} color="green"/>
        } else {
            return  <Image source={CatIcons.getIcon(icon)}
                style={{width:40, height: 40}}
            ></Image>
        }
    }

    _renderWalletRow = (item) => {
        return (
            <TouchableOpacity 
                        key = {'wallet-' + item.id}
                        onPress = {() => this._onWalletSelect(item)} 
                        style={styles.walletRow} activeOpacity={.6}>
                {this._renderIcon(item.icon)}
                <View style={styles.walletName}>
                    <Text style={styles.title}>{item.name}</Text>
                    <Text style={styles.subtitle}>Wallet</Text>
                </View>
            </TouchableOpacity>
        )
    }
    render() {
        const width = Dimensions.get('window').width - 80;
        const height = Dimensions.get('window').height - 300;
        
        return (
                <Modal
                    visible = {this.props.isVisible}
                    onRequestClose = {() => this.props.closeModal()}
                    transparent = {true}
                    style = {styles.modal}
                    // animationType="slide"
                   
                >
                    <View style = {styles.container} >
                        <View style={[styles.content, {width: width}]}>
                            <Text style={styles.modalTitle}>{this.state.title } Wallet</Text>
                            <ScrollView style={[styles.walletList, {maxHeight: height}]}>
                                {
                                    Object.values(this.props.wallets).map(value => {
                                        return this._renderWalletRow(value);
                                    })
                                }
                            </ScrollView>
                            <TouchableOpacity onPress={() => this.props.closeModal()}>
                                <Text style={styles.close}>Cancel</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>
            
        )
    }
}


const styles = StyleSheet.create({
    modal: {
        flex: 1,
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(0, 0, 0, .3)',
    },
    content: {
        // padding: 18,
        backgroundColor: 'white',
        elevation: 4,
        borderRadius: 3,
        overflow: 'hidden',
    },
    modalTitle:  {
        backgroundColor: Color.GREY[100],
        paddingHorizontal: 18,
        paddingVertical: 18,
    },

    walletRow: {
        flexDirection: 'row',
        paddingHorizontal: 18,
        paddingVertical: 12,
        alignItems: 'center',
        borderBottomColor: Color.GREY[400],
        borderBottomWidth: .7,
    },
    walletName: {
        flex: 1,
        marginLeft: 12,
    },
    title: {
        fontSize: 16,
    },
    subtitle: {
        fontSize: 12,
        color: Color.GREY[400],
    },
    close: {
        alignSelf: 'flex-end',
        paddingHorizontal: 18,
        paddingVertical: 12,
        fontSize: 14,
        color: Color.GREY[500],
    },
    walletList: {

    },

})