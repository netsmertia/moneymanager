import React, {Component} from "react";
import {Text, View, StyleSheet} from "react-native";
import Color from "react-native-material-color";
import moment from "moment";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
export default class SectionListHeader extends Component {
    render () {
        const section = this.props.section;
        const date = section.header.title;
        return (
            <View style={styles.sectionHeader}>
                <View style={styles.headerLeft}>
                    <Text style={styles.dateDay}>{moment(date).format('DD')}</Text>
                    <View>
                        <Text style={styles.dateDayName}>{moment(date).format('dddd')}</Text>
                        <Text style={styles.dateMonth}>{moment(date).format('MMM YYYY')}</Text>
                    </View>
                </View>
                <View style={{flexDirection: 'column', justifyContent: 'center', alignItems: 'flex-end'}}>
                    <View style={styles.amtLabel}>
                        <Text style={{fontSize: 10, fontStyle: 'italic', }}>Inflow: </Text>
                        <Icon name="currency-inr" ></Icon>
                        <Text style={{fontSize: 10, fontStyle: 'italic', }}>{parseFloat(section.header.total.inflow).toFixed(2)}</Text>
                    </View>

                    <View style={styles.amtLabel}>
                        <Text style={{fontSize: 10, fontStyle: 'italic', }}>Outflow: </Text>
                        <Icon name="currency-inr" ></Icon>
                        <Text style={{fontSize: 10, fontStyle: 'italic', }}>{parseFloat(section.header.total.outflow).toFixed(2)}</Text>
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({

    sectionHeader: {
        height: 30,
        paddingHorizontal: 18,
        backgroundColor: Color.GREY[200],
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    headerLeft: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    dateDay: {
        fontSize: 24,
        marginRight: 8,
    },
    dateDayName: {
        fontSize: 10,
        marginBottom: -2,
    },
    dateMonth: {
        fontSize: 10,
    },
    
    monthSelectorButton: {
        paddingHorizontal: 30,
    },
    amtLabel: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: -1,
        marginTop: -1,
    }

});

