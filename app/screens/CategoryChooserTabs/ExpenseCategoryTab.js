import React, {Component} from "react";
import {Text, View, StyleSheet, TouchableOpacity, Image, FlatList, Keyboard} from "react-native";
import Color from "react-native-material-color";
import CatIcons from "../../IconList";
import {Category} from "../../models";
import {NavigationActions} from "react-navigation";

export default class ExpenseCategoryTab extends Component {

    constructor(props) {
        super(props);
        this.state = {
            categories: {},
            isNew: false,
        }
    }

    componentWillMount() {
        Category.getCategoriesByType('expense', data => {
            this.setState({categories: data});
        });
        const {isNew} = this.props.screenProps;
        if (isNew != undefined) {
            this.setState({isNew: isNew});
        }
        // if (defaultTab != undefined) {
        //     // const action = NavigationActions.reset({
        //     //     index:0,
        //     //     actions: [NavigationActions.navigate({routeName: defaultTab})],
        //     // })
        //     console.log(this.props.navigation);
        //     // this.props.navigation.re
        // }
    }

    _renderListItem = (item) => {
        return (

            <TouchableOpacity activeOpacity={.6} style={styles.row} onPress={() => this._onItemPress(item)}>
                <Image source={CatIcons.getIcon(item.icon)} style={{width:40, height: 40}}></Image>
                <Text style={styles.title}>{item.name}</Text>
            </TouchableOpacity>
        )
    }
    _onItemPress = (item) => {
        const {pn, updateSelectedCategory}  = this.props.screenProps;

        if (this.state.isNew) {
            pn.navigate({key: 'addTransaction', routeName: 'AddTransaction', params: {category: item, txType: 'expense'}});
        } else {
            updateSelectedCategory(item);
            pn.goBack(null);
        }
    }
    render() {
        return(
            <View>
                <FlatList
                    keyExtractor = {(item, index)=> 'x' + index}
                    data = {this.state.categories}
                    renderItem = {({item}) => this._renderListItem(item)}
                    
                ></FlatList>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 18,
        paddingVertical: 12,
        borderBottomWidth: .7,
        borderColor: Color.GREY[400],
    },
    title: {
        marginLeft: 18,
    }
});