import React, {Component} from "react";
import {Text, View, StyleSheet, Image, TouchableOpacity, ScrollView, StatusBar, ToastAndroid} from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import Color from "react-native-material-color";
import Realm  from "realm";
import moment from "moment";

import TCard from "../components/TransactionCard";
import TSCard from "../components/TopSpendingCard";
import CatIcons from "../IconList";
import AnimatedCircularProgress from "../components/AnimatedCircularProgress";
import WalletHomeCard from "../components/WalletHomeCard";
import {Category, Wallet, Transaction } from "../models";

import firebase from "react-native-firebase";
export default class HomeScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            test: 'this is test',
            test2: '',
            today: (new Date()).toISOString(),
            trans: [],
            wallet: {},
            totalSpend: 0,
            topSpendings: [],
            isAuthenticated: false,
        }
    }

    componentWillMount() {
        this._updatePageData();
        Wallet.setupInitialWallet(() => ToastAndroid.show('wallet setup done', ToastAndroid.SHORT));
        Category.setupInitialCategory(() => ToastAndroid.show('Category setup done', ToastAndroid.SHORT));
        // Category.getCategories(data => {alert(JSON.stringify(data))});
        // Wallet.getAllWallets(data => alert(JSON.stringify(data)));
        // Transaction.seed(() => alert('done'));
        // Transaction.getRecentTransactions(data => alert(JSON.stringify(data)));
        this.props.navigation.addListener('willFocus', this._willFoucus);
        console.log('component will mount');
    }

    _willFoucus = () => {
        this._updatePageData();
    }
    
    goToAddTransaction = () => {
        this.props.navigation.navigate({key: 'categoryChooser', routeName: 'CategoryChooser', params: {isNew: true, test: 'testing', updateList: this._updateList, defaultTab: 'ExpenseCategoryTab'}});
    }

    _updateList = () => {
        this._updatePageData();
    }

    _updatePageData = () => {
        Transaction.getRecentTransactions(data => {
            this.setState({trans: data});
        })

        Transaction.getCategoryWiseSpendings(this.state.today, data => {
            this.setState({topSpendings: data.slice(0, 5)});
        });

        Transaction.getTotalSpending(this.state.today, data => {
            this.setState({totalSpend: data});
        });

        Wallet.getWalletWiseTotal(this.state.today, data => {
            this.setState({wallet: this._groupByWalletType(data)});
        });
   }
    _onTransactionClick = (trans) => {
        this.props.navigation.navigate({key: 'editTransaction', routeName: 'EditTransaction', params: {transaction: trans, updateList: this._updateList, pageData: {}}});
    }
    _onAllTransactionsClick = () => {
        // alert('clicked');
        this.props.navigation.navigate({key: 'allTransactions', routeName: 'AllTransactions', params: {}});
    }
    _onCategoryClick = (item) => {
        this.props.navigation.navigate({key: 'categorySpendings', routeName: 'CategorySpendings', params: {category: item}});
    }
    _onAllCategoriesClick = () => {
        this.props.navigation.navigate({key: 'allCategoriesSpendings', routeName: 'AllCategoriesSpendings', params: {}});
    }
    _onWalletsClick =  () => {
        this.props.navigation.navigate({key: 'Wallets', routeName: 'Wallets'});
    }
    _groupByWalletType = (data) => {
        return Object.values(data).map((r, i) => {
            r[i.walletType] = r[i.walletType] || {
                walletType : i.walletType,
                inflow: 0,
                outflow: 0,
                walletIcon: i.walletIcon,
                walletCount: 0,
            };
            r[i.walletType].inflow += i.inflow;
            r[i.walletType].outflow += i.outflow;
            r[i.walletType].walletCount ++;
            return r;
        }, {});
    }
    _renderWallet = (wallets, type) => {
        if (wallets[type] != undefined) {
            return <WalletHomeCard item = {wallet[value]}/>
        }
    }
    render() {
        const gtotal = Object.values(this.state.topSpendings).reduce((r, i) => r = r + i.expense, 0);
        return (
            <ScrollView>
                <StatusBar backgroundColor = {Color.GREEN[800]} />
                {/* <Text>{gtotal}</Text> */}
                {/* <Text>test dates : {JSON.stringify(this.state.test2)}</Text> */}
                <View style={styles.topCard}>
                    <View style={styles.cardHeader}>
                        <View>
                            <AnimatedCircularProgress
                                size={160}
                                width={5}
                                fill={50}
                                tintColor= {Color.GREEN[400]}
                                backgroundColor="#3d5875"
                                rotation={180}
                                renderChild = {
                                    (fill) => (
                                        <View style={{justifyContent: 'center', alignItems: 'center'}}>
                                            <Text style={{color: 'green', fontSize: 22}}>{parseFloat(fill).toFixed(2)}%</Text>
                                            <Text style={{color: 'black'}}>Total Spending</Text>
                                            <Text style={{color: 'gray', fontSize: 16}}>{parseFloat(this.state.totalSpend).toFixed(2)}</Text>
                                        </View>
                                    )
                                }
                            > 
                            </AnimatedCircularProgress>
                        </View>
                    </View>
                    <TouchableOpacity style={styles.walletSection}
                        onPress={this._onWalletsClick}
                    >
                        {
                            Object.values(this.state.wallet).sort((a, b) => a.item == 'cash' ? -1 : 1).map(value => {
                                if (value.walletType == 'cash' || value.walletType == 'online') {
                                    return <WalletHomeCard key = {'wallet-' + value.walleType} item = {value}/>
                                }
                            })
                        }
                    </TouchableOpacity>

                    <View style={styles.cardFooter}>
                        <Text style={styles.month}>March, 2018</Text>
                        <TouchableOpacity style={styles.addBtn} onPress={this.goToAddTransaction}>
                            <Icon name="plus-circle" size={24} />
                            <Text style={{paddingLeft: 4,}}>ADD</Text>
                        </TouchableOpacity>
                    </View>
                </View>


                <View>

                    <View style={styles.recentTransBox}>
                        <Text style={styles.recentTransTitle}>Recent Transactions </Text>
                        {
                            this.state.trans.map(item => {
                                return <TCard
                                   key={item.id}
                                   data = {item}
                                   onPress = {() => this._onTransactionClick(item)}
                                />
                            })
                        }
                        <View style={styles.moreRow} >
                            <TouchableOpacity
                                style={{flexDirection: 'row'}}
                                onPress = {this._onAllTransactionsClick}
                            >
                            <Text>More</Text>
                            <Icon size={20} name="chevron-down"></Icon>
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style={styles.topSpendingBox}>
                        <View style={styles.topSpendingBoxTitle}>
                            <Text style={{fontSize: 20}}>Top Spendings</Text>
                            <Text style={{fontStyle: 'italic'}}>{moment(new Date).format('MMM YYYY')}</Text>
                        </View>
                        
                         {
                             this.state.topSpendings.map(item => {
                                 return <TSCard 
                                            key = {item.id} 
                                            item = {item}
                                            gtotal ={gtotal}
                                            onPress = {() => this._onCategoryClick(item)}
                                        />;
                             })
                         }
                        <View style={styles.moreRow} >
                            <TouchableOpacity 
                                style={{flexDirection: 'row'}}
                                onPress={this._onAllCategoriesClick}
                            >
                            <Text>More</Text>
                            <Icon size={20} name="chevron-down"></Icon>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>


            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({

    topCard: {
        // justifyContent: 'center',
        // alignItems: 'center',
        minHeight: 100,
        backgroundColor: Color.GREY[100],
        elevation: 4,
        margin: 10,
    },

    cardHeader: {
        padding: 18,
        alignItems: 'center',
    },
    walletSection: {
        // padding: 18,
        // flexDirection: 'row',
    },

    
    amtLine: {
        fontSize: 20,
    },

    amt: {
        fontSize: 28,
        fontWeight: 'bold',
        paddingLeft: 8,
    },
    targetLine: {
        fontSize: 16,
        color: '#9b9b9b',
    },
    targetAmt: {
        fontSize: 20,
        color: '#9b9b9b',
    },
    cardFooter: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: Color.GREY[600],
        width: '100%',
        paddingHorizontal: 18,
        paddingVertical: 10,
    },
    month: {
        color: '#777',
        fontSize: 18,
        color: 'white',
    },
    addBtn: {
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        backgroundColor: Color.GREEN[400],
        borderRadius: 40,
        padding: 2,
        paddingHorizontal: 4,
        paddingRight: 10,
    },

    recentTransBox: {
        margin: 10,
        elevation: 4,
        backgroundColor: 'white',
    },
    recentTransTitle: {
        fontSize: 20,
        paddingHorizontal: 18,
        paddingVertical: 12,
        backgroundColor: Color.GREY[100],
    },

    topSpendingBox: {
        margin: 10,
        elevation: 4,
        backgroundColor: 'white',
    },

    topSpendingBoxTitle: {
        flexDirection: 'row',
        paddingHorizontal: 18,
        paddingVertical: 12,
        backgroundColor: Color.GREY[100],
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    moreRow: {
        alignItems: 'center',
        justifyContent: 'flex-end',
        flexDirection: 'row',
        paddingHorizontal: 18,
        paddingVertical: 12,
    }

})