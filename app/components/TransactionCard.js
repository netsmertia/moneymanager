import React, {Component} from 'react';
import {Text, View, StyleSheet, Image, TouchableOpacity} from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import Color from "react-native-material-color";
import CatIcons from "../IconList";
import moment from "moment";
import Config from "../Config";
import PropTypes from "prop-types";
export default class TransactionCard extends Component {
    _amtColor = () => {
        switch(this.props.data.type) {
            case Config.transactionType.expense: 
                return Color.PINK[400];
            case Config.transactionType.income:
                return Color.LIGHTGREEN[700];
            case Config.transactionType.transfer: 
                return Color.GREY[500];
            default:
                return Color.GREY[500];
        }
    }
    render() {
        const data = this.props.data;
        const color = this._amtColor();
        return (
            <TouchableOpacity style={styles.container}
                activeOpacity={.7}
                onPress={this.props.onPress}
             >
                <Image source={CatIcons.getIcon(data.category.icon)} style={{width: 40, height: 40}}></Image>
                <View style={styles.textBox}>
                    <Text style={styles.heading}>{data.category.name}</Text>
                    <Text style={styles.subheading}>{data.title}</Text>
                </View>
                <View style={styles.amtBox}>
                    <Text style={[styles.amt, {color: color}]}>₹ {parseFloat(data.amt).toFixed(2)}</Text>
                    <Text style={styles.date}>{moment(data.date).format('DD MMM')}</Text>
                </View>
            </TouchableOpacity>
        );
    }
}

TransactionCard.propTypes = {
    data: PropTypes.object.isRequired,
}

const styles = StyleSheet.create({
    container: {
        height: 65,
        paddingHorizontal: 18,
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomWidth: 1,
        borderColor: Color.GREY[200],
        backgroundColor: 'white',
    },
    textBox: {
        marginLeft: 16,
        flex: 1,
    },
    heading: {
        fontSize: 16,
        color: Color.GREY[800],
    },
    subheading: {
        fontSize: 14,
        color: Color.GREY[400],
    },
    amt: {
        fontSize: 18,
        // color: Color.LIGHTGREEN['700'],
        color: Color.PINK[400],
        textAlign: 'right',
        // fontWeight: 'bold',
    },
    date: {
        fontSize: 14,
        color: Color.GREY[400],
        textAlign: 'right',
    }
});