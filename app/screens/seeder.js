import React, {Component} from "react";
import {Text, View, Button} from "react-native";
import {Transaction} from "../models";
export default class Seeder extends Component {
    _seedTransactions = () => {
        Transaction.seed(() => {
            alert('success');
        });
    }
    render() {
        return (
            <View style = {{padding: 20}}>
                <Button
                    title = "Seed Transactions"
                    onPress = {this._seedTransactions}
                ></Button>
            </View>
        );
    }
}