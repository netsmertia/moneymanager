import React, {Component} from "react";
import {Text, View, StyleSheet, TextInput, Image, TouchableOpacity, DatePickerAndroid, Keyboard} from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import Color from "react-native-material-color";
import moment from "moment";
import CatIcons from "../IconList";

import {Category, Transaction, Wallet} from "../models";
import WalletSelectorModal from "../components/WalletSelectorModal";
import Utils from "../Utils";
import Config from "../Config";
export default class AddTransaction extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: '',
            date: new Date(),
            amt: undefined,
            category: {},
            wallet: {},
            txType: '',
            // fromWallet: {},
            isModalVisible: false,
            isToWalletModalVisible: false,
            walletList: {},
            toWallet: {},
            pageCall: 'addTransaction',
        }
    }
    static navigationOptions = ({navigation}) => {
        const params = navigation.state.params || {};
        return {
            headerRight: <TouchableOpacity onPress={params.onSave}><Text style={{color: 'white', padding: 16, fontSize: 16,}}>Save</Text></TouchableOpacity>
        }
    };


    componentWillMount() {
        this.props.navigation.setParams({onSave: this._onSave});
        const { category, txType, toWallet } = this.props.navigation.state.params;
        this.setState({category: category, txType: txType});
        Wallet.getAllWallets(data => {
            this.setState({walletList: data});
            if (toWallet && Object.keys(data).length > 1) {
                fromWallets = Object.values(data).filter(item => item.id != toWallet.id);
                console.log(fromWallets);
                this.setState({wallet: fromWallets[0]});
                this.setState({toWallet: toWallet});
            } else {
                this.setState({wallet: data[0]});
            }
            // this.setState({wallet: wallet ? wallet : data[0]});
        });
    }

    _onSave = () => {
        // this.props.navigation.goBack('categoryChooser');

        if (this.state.amt == undefined) {
            alert('Please enter amount');
            return;
        };

        if (Object.keys(this.state.category).length == 0) {
            alert('Please select a category');
            return;
        }

        if (Object.keys(this.state.wallet).length == 0) {
            const type = this.state.txType == Config.transactionType.income ? 'To' : 'From';
            alert('Please select ' + type + ' Wallet');
            return;
        }
        if (this.state.txType == Config.transactionType.transfer 
                && Object.keys(this.state.toWallet).length == 0) {
            alert('Please select To Wallet');
            return;
        }

        this.setState({pageCall: 'HomePage'});
        if (this.state.pageCall != 'HomePae') {
            Transaction.addTransaction({
                title: this.state.title,
                amt: Utils.toAmt(this.state.amt),
                category: this.state.category,
                date: this.state.date,
                createdAt: new Date(),
                type: this.state.txType,
                wallet: this.state.wallet,
                toWallet: Object.keys(this.state.toWallet).length == 0 ? undefined : this.state.toWallet,
            }, () => {

                // this.props.navigation.state.params.updateList();
                this.props.navigation.goBack('categoryChooser');
            })
        }
    }
    _selectCateogry = () => {
        Keyboard.dismiss();
        this.props.navigation.navigate({key: 'categoryChooser', routeName: 'CategoryChooser', params: {updateSelectedCategory: this._updateSelectedCategory}});
    }
     _selectDate = async () => {
        try {
            const {action, year, month, day}  = await DatePickerAndroid.open({
                date: new Date(),
            });
            if (action !== DatePickerAndroid.dismissedAction) {
                this.setState({date: new Date(year, month, day).toISOString()});
            }
        } catch({code, message}) {
            alert('some thing went worng ' + message);
        }
    }
    _selectWallet = () => {
        this.setState({isModalVisible: true});
    }

    _selectToWallet = () => {
        this.setState({isToWalletModalVisible: true});
    }
    _onWalletSelect = (item) => {
        if (item.id == this.state.wallet.id) {
            return;
        } 
        if (this.state.txType == Config.transactionType.transfer 
                    && Object.keys(this.state.walletList).length > 1 ) {
            const walletList = Object.values(this.state.walletList).filter(i => i.id != item.id);
            this.setState({toWallet: walletList[0]});
        }
        this.setState({wallet: item});

    }

    _onToWalletSelect = (item) => {
        if (item.id == this.state.toWallet.id && this.state.txType != Config.transactionType.transfer) {
            return;
        } 
        const walletList = Object.values(this.state.walletList).filter(i => i.id != item.id);
        this.setState({wallet: walletList[0]});
        this.setState({toWallet: item});
    }
    _closeModal = () => {
        this.setState({isModalVisible: false});
    }

    _closeToWalletModal = () => {
        this.setState({isToWalletModalVisible: false});
    }
    _updateSelectedCategory = (item) => {
        this.setState({category: item})
    }

    _renderCatIcon = () => {
        // iconFile = '../img/shopping.png';
        iconFile = this.state.category.icon;
        if (iconFile == undefined) {
            return <Icon style={styles.leftIcon} name="help-circle" size={32} color="green"/>
        } else {
            return  <Image source={CatIcons.getIcon(iconFile)}
                style={{width:40, height: 40}}
            ></Image>
        }
    }

    _renderIcon = (icon) => {
        // iconFile = '../img/shopping.png';
        if (icon == undefined) {
            return <Icon style={styles.leftIcon} name="help-circle" size={32} color="green"/>
        } else {
            return  <Image source={CatIcons.getIcon(icon)}
                        style={{width:40, height: 40}} />
        }
    }

    _renderToWallet = (type, item) => {
        if (Object.keys(this.state.walletList).length > 1)
        if (type == Config.transactionType.transfer && Object.keys(item) != 0) {
            return (
                <TouchableOpacity 
                    onPress={this._selectToWallet}
                    style={[styles.boxRow]}>
                    {this._renderIcon(item.icon)}

                    <View style={styles.captionBox}>
                        <Text style={styles.caption}>{type == 'transfer' ? 'To' : 'From' } Wallet</Text>
                        <Text style={styles.heading}>{Object.keys(item).length == 0 ? 'Select Wallet' : item.name}</Text>
                    </View>
                </TouchableOpacity>
            )
        }
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.amtCard}>
                    <Icon name="currency-inr" size={32} color="green"/>
                    <TextInput
                        style={styles.amtInput}
                        underlineColorAndroid="transparent"
                        placeholder="Enter Amount"
                        keyboardType='numeric'
                        onChangeText={(text) => this.setState({amt: parseFloat(text).toFixed(2)})}
                    ></TextInput>
                </View>

                <View style={{paddingHorizontal: 0}}>

                    <View style={[styles.boxRow]}>
                        <Icon style={styles.leftIcon} name="comment-text" size={32} color="green"/>
                        <TextInput placeholder="Note" 
                            underlineColorAndroid="transparent"
                            onChangeText={(text) => this.setState({title: text})}
                        style={styles.bigText}></TextInput>
                    </View>

                    <TouchableOpacity onPress={this._selectDate} style={[styles.boxRow]}>
                        <Icon style={styles.leftIcon} name="calendar" size={32} color="green" />
                        {/* <Text style={styles.bigText}>{moment(this.state.transaction.date).startOf('day').diff(moment(new Date()).startOf('day'), 'days')}</Text> */}
                        <Text style={styles.bigText}>{moment(this.state.date).startOf('day').diff(moment(new Date()).startOf('day'), 'days') == 0 ? 'Today' : moment(this.state.date).format('DD MMM YYYY')}</Text>
                    </TouchableOpacity>

                    <TouchableOpacity 
                        onPress={this._selectCateogry}
                        style={[styles.boxRow]}>
                        {this._renderCatIcon()}
                        <View style={styles.captionBox}>
                            <Text style={styles.caption}>Category</Text>
                            <Text style={styles.heading}>{Object.keys(this.state.category).length == 0 ? 'Select Category' : this.state.category.name}</Text>
                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity 
                        onPress={this._selectWallet}
                        style={[styles.boxRow]}>
                        {this._renderIcon(this.state.wallet.icon)}

                        <View style={styles.captionBox}>
                            <Text style={styles.caption}>{this.state.txType == 'income' ? 'To' : 'From' } Wallet</Text>
                            <Text style={styles.heading}>{Object.keys(this.state.wallet).length == 0 ? 'Select Wallet' : this.state.wallet.name}</Text>
                        </View>
                    </TouchableOpacity>
                   
                   {
                       this._renderToWallet(this.state.txType, this.state.toWallet)
                   }
                </View>
                <Text>{JSON.stringify(this.state)}</Text>
                <WalletSelectorModal 
                        walletType = "from"
                        txType = {this.state.txType}
                        isVisible = {this.state.isModalVisible} 
                        closeModal = {this._closeModal}
                        wallets = {this.state.walletList}
                        onWalletSelect = {this._onWalletSelect}
                        />

                <WalletSelectorModal 
                        walletType = "to"
                        txType = {this.state.txType}
                        isVisible = {this.state.isToWalletModalVisible} 
                        closeModal = {this._closeToWalletModal}
                        wallets = {this.state.walletList}
                        onWalletSelect = {this._onToWalletSelect}
                        />

            </View>
        )
    }
}



const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    saveBtn: {
        color: 'white',
    },
    amtCard: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 28,
        backgroundColor: Color.GREY[300],
        // marginTop: 30,
        
    },
    amtInput:{
        width: 200,
        fontSize: 30,
    },
    leftIcon: {
        width: 40,
    },
    bigText: {
        fontSize: 20,
        marginLeft: 18,
        flex: 1,
    },
    boxRow: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        height: 70,
        paddingHorizontal: 18,
        borderBottomWidth: .7,
        borderColor: Color.GREY[400],
    },
    captionBox: {
        marginLeft: 18,
        flex: 1,
    },
    caption: {
        fontSize: 12,
        color: Color.GREY[400],
    },
    heading: {
        fontSize: 20,
    }

});