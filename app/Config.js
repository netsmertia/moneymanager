class Config {
    static categoryType = {
        income: 'income',
        expense: 'expense',
        transfer: 'transfer',
    }
    static transactionType = {
        income: 'income',
        expense: 'expense',
        transfer: 'transfer',
    }
    static walletType = {
        cash : 'cash',
        online: 'online',
        bank : 'bank',
        // debt : 'debt',
        // saving : 'saving',
    }

    static defaultWallets = [
        {
            name : 'Cash',
            type: Config.walletType.cash,
            icon: 'wallet',
            balance: 0,
        },

        {
            name : 'Online',
            type: Config.walletType.online,
            icon: 'creditcard',
            balance: 0,
        }
    ]
     
    static defaultCategories = [
        {id: 1, name: 'Bills', icon: 'bills' , type: 'expense'},
        {id: 2, name: 'Shopping', icon: 'shopping', type: 'expense'},
        {id: 3, name: 'Household', icon: 'bucket', type: 'expense'},
        {id: 4, name: 'Groceries', icon: 'groceries', type: 'expense'},
        {id: 5, name: 'Gifts', icon: 'gift2', type: 'expense'},
        {id: 6, name: 'Laundary', icon: 'shirt', type: 'expense'},
        {id: 7, name: 'Saving', icon: 'saving', type: 'expense'},
        {id: 8, name: 'Family', icon: 'user', type: 'expense'},
        {id: 9, name: 'Cash', icon: 'wallet', type: 'expense'},
        {id: 10, name: 'Service', icon: 'service', type: 'expense'},
        {id: 11, name: 'Studies', icon: 'library', type: 'expense'},
        {id: 12, name: 'Card', icon: 'creditcard', type: 'expense'},
        {id: 13, name: 'Electronics', icon: 'camera', type: 'expense'},
        {id: 15, name: 'Home', icon: 'house', type: 'expense'},
        {id: 16, name: 'Salary', icon: 'money', type: 'income'},
        {id: 17, name: 'Gift', icon: 'gift', type: 'income'},
        {id: 18, name: 'Rent', icon: 'house', type: 'income'},
        {id: 19, name: 'Sale', icon: 'service', type: 'income'},
        {id: 20, name: 'A/c to A/c', icon: 'transfer', type: 'transfer'},
    ];
}

export default Config;