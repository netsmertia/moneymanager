import React, {Component} from "react";
import {Text, View, StyleSheet, TouchableOpacity, Image, FlatList, Keyboard} from "react-native";
import Color from "react-native-material-color";
import CatIcons from "../../IconList";
import {Wallet, Category} from "../../models";
import Config from "../../Config";

export default class WalletChooserTab extends Component {

    constructor(props) {
        super(props);
        this.state = {
            wallets: {},
            category: {},
            txType: Config.transactionType.transfer,
        }
    }

    componentWillMount() {
        Wallet.getAllWallets(data => {
            console.log(data);
            this.setState({wallets: data});
        });

        Category.getCategoriesByType(Config.categoryType.transfer, data => {
            console.log(data);
            this.setState({category: data[0] || {}});
        });

        const {isNew} = this.props.screenProps;
        if (isNew != undefined) {
            this.setState({isNew: isNew});
        }
    }

    _renderListItem = (item) => {
        return (

            <TouchableOpacity activeOpacity={.6} style={styles.row} onPress={() => this._onItemPress(item)}>
                <Image source={CatIcons.getIcon(item.icon)} style={{width:40, height: 40}}></Image>
                <Text style={styles.title}>To {item.name} Wallet</Text>
            </TouchableOpacity>
        )
    }
    _onItemPress = (item) => {

        const {pn, updateToWallet}  = this.props.screenProps;

        if (this.state.isNew) {
            pn.navigate({key: 'addTransaction', routeName: 'AddTransaction', params: { category: this.state.category, txType: this.state.txType, toWallet: item}});
        } else {
            updateToWallet(item, this.state.category);
            pn.goBack(null);
        }
    }
    render() {
        return (
            <View>
                <FlatList
                    keyExtractor = {(item, index)=> 'x' + index}
                    data = {this.state.wallets}
                    renderItem = {({item}) => this._renderListItem(item)}
                    
                ></FlatList>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 18,
        paddingVertical: 12,
        borderBottomWidth: .7,
        borderColor: Color.GREY[400],
    },
    title: {
        marginLeft: 18,
    }
});