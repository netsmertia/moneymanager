import {CategorySchema, TransactionSchema, WalletSchema} from "./Schema";
import Realm, { schemaVersion } from "realm";

export default class DBHelper {
    static open = (callback) => {
        Realm.open({
            schema: [CategorySchema, TransactionSchema, WalletSchema],
            schemaVersion: 18,
            migration: (oldRealm, newRealm) => {
                newRealm.deleteAll();
            }
        })
        .then(realm => {
            callback(realm);
        });
    }
}